#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#define BB while(getchar()!='\n')
#define MAX_TIPUS 35
#define MAX_NOM 15
#define str(x) #x
#define xstr(x) str(x)
#define NOM_FITXER "animals.dat"
#define ERROR_OBRIR_FITXER "Error en obrir el fitxer"
#define ERROR_ESCRIURE_FITXER "Error d'escriptura"
#define ERROR_LLEGIR_FITXER "Error de lectura"
#define ERROR_OPERACIO_CANCELADA "Operacio Cancelada"
#define ERROR_NO_BORRAR "No s'ha pogut borrar el fitxer"
#define ERROR_RENOM_FITXER "No ha pogut renombrar el fitxer"
#define INFO_NO_ERROR "Totes les operacions realitzades amb exit"
#define ERROR_OPERACIO_NO_DISPONIBLE "No es pot realitzar aquesta tasca"
#define CONTINUAR "Prem ENTER per continuar..."

typedef struct{
    char tipus[MAX_TIPUS+1];
    char nom[MAX_NOM+1];//No es pot repetir, es el camp CLAU
    char sexe;//M-Masculí, F-Femení
    bool enPerillExtincio;
    int edat;
    double pes;
    char marcaEsborrat;
}Animal;

void pintarMenu();
void entraOpcio(int *opcio);
void entrarAnimal(Animal *animal, bool modifica);
void escriureAnimal(Animal animal);
int alta(char nomFitxer[]);
int consulta(char NomFitxer[], bool esborrats);
int baixaOModifica(char NomFitxer[], bool esModifica);
bool seguirBaixaOModifica(bool esModifica);
int esborrarFitxer(char NomFitxer[]);
int compactarFitxer(char nomFitxer[]);
int esborrarFitxerGeneric(char nomFitxer[]);
int numeroRegistres(char nomFitxer[], long *registre);
int accesDirecte(char nomFitxer[]);
int crearInforme(char nomFitxer[]);

int main()
{
    int opcio,error;
    long registre;
    do{
        pintarMenu();
        entraOpcio(&opcio);
        error = 0;
        switch(opcio){
            case 1:
                error = alta(NOM_FITXER);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -2)printf(ERROR_ESCRIURE_FITXER);
                break;
            case 2:
                error = baixaOModifica(NOM_FITXER, false);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -2)printf(ERROR_ESCRIURE_FITXER);
                if(error == -3)printf(ERROR_LLEGIR_FITXER);
                if(error == -4)printf(ERROR_OPERACIO_CANCELADA);
                break;
            case 3:
                error = consulta(NOM_FITXER, false);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -3)printf(ERROR_LLEGIR_FITXER);
                break;
            case 4:
                error = baixaOModifica(NOM_FITXER, true);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -2)printf(ERROR_ESCRIURE_FITXER);
                if(error == -3)printf(ERROR_LLEGIR_FITXER);
                if(error == -4)printf(ERROR_OPERACIO_CANCELADA);
                break;
            case 5:
                error = consulta(NOM_FITXER, true);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -3)printf(ERROR_LLEGIR_FITXER);
                break;
            case 6:
                error = esborrarFitxerGeneric(NOM_FITXER);
                printf("\n");
                if(error == 0){printf(INFO_NO_ERROR); getchar();}
                if(error == -4)printf(ERROR_OPERACIO_CANCELADA);
                if(error == -6)printf(ERROR_NO_BORRAR);
                break;
            case 7:
                error = compactarFitxer(NOM_FITXER);
                if(error == 0){printf(INFO_NO_ERROR); getchar();}
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -3)printf(ERROR_LLEGIR_FITXER);
                if(error == -5)printf(ERROR_RENOM_FITXER);
                break;
            case 8:
                error = numeroRegistres(NOM_FITXER,&registre);
                printf("Hi ha %ld registres actualment", registre);
                printf("\n"CONTINUAR"\n");
                getchar();
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -3)printf(ERROR_LLEGIR_FITXER);
                break;
            case 9:
                error = accesDirecte(NOM_FITXER);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -7)printf(ERROR_OPERACIO_NO_DISPONIBLE);
                break;
            case 10:
                error = crearInforme(NOM_FITXER);
                if(error == -1)printf(ERROR_OBRIR_FITXER);
                if(error == -2)printf(ERROR_ESCRIURE_FITXER);
                if(error == -3)printf(ERROR_LLEGIR_FITXER);
                if(error == -4)printf(ERROR_OPERACIO_CANCELADA);
                break;
            case 0:
                break;
            default:
                printf("\n\tLes entrades valides son de 0 a 7 \n");
        }
        if(error != 0){printf("\n"CONTINUAR"\n"); getchar();}
    }while(opcio != 0);
    return 0;
}

void pintarMenu(){
    fflush(stdout);
    system("cls || clear");
    printf("\n\t** MENU **\n"
           "\n\t1- Alta\n"
           "\n\t2- Baixa\n"
           "\n\t3- Consulta\n"
           "\n\t4- Modificacions\n"
           "\n\t5- Consulta esborrats\n"
           "\n\t6- Esborrar fitxer\n"
           "\n\t7- Compactar fitxer\n"
           "\n\t8- Numero de registres\n"
           "\n\t9- Acces directe\n"
           "\n\t10- Informe\n"
           "\n\n"
           "\n\t0- Sortir\n");
    printf("\n\n\tTria una Opcio (0-10): ");
}

void entraOpcio(int *opcio){
    scanf("%d", opcio);BB;
}

void entrarAnimal(Animal *animal, bool modifica){
    system("cls || clear");
    char enPerill;
    printf("\nIntrodueix el tipus: ");
    scanf("%"xstr(MAX_TIPUS)"[^\n]",animal->tipus);BB;
    if(!modifica){
        printf("\nIntrodueix el nom: ");
        scanf("%"xstr(MAX_NOM)"[^\n]",animal->nom);BB;
    }
    do{
        printf("\nIntrodueix el sexe (m/f): ");
        scanf("%c",&animal->sexe);BB;
    }while(animal->sexe != 'm' && animal->sexe != 'f');

    do{
        printf("\nEsta en perill d'extincio? (s/n): ");
        scanf("%c",&enPerill);BB;
    }while(enPerill != 's' && enPerill != 'n');//o es 'n' o es 's'
    animal->enPerillExtincio = (enPerill == 's')?true:false;

    printf("\nIntrodueix l'edat: ");
    scanf("%d",&animal->edat);BB;
    printf("\nIntrodueix el pes: ");
    scanf("%lf",&animal->pes);BB;
}

void escriureAnimal(Animal animal){
    printf("\nEl tipus: %s", animal.tipus);
    printf("\nEl nom: %s",animal.nom);
    if(animal.sexe == 'f')printf("\nL'animal es femella");
    else printf("\nL'animal es mascle");
    if(animal.enPerillExtincio == true) printf("\nEsta en perill d'extincio: Si");
    else printf("\nEsta en perill d'extincio: No");
    printf("\nEdat: %d",animal.edat);
    printf("\nPes: %.3lf\n",animal.pes);
}

int alta(char nomFitxer[]){
    Animal a1;
    FILE *f1;
    int n;
    //obrir per escriure en binari. Si existeix el matxaca sinó el crea.
    f1 = fopen(nomFitxer,"ab");
    if(f1 == NULL) return -1;
    entrarAnimal(&a1, false);
    n = fwrite(&a1,sizeof(Animal),1,f1);
    if(n == 0) return -2;
    fclose(f1);
    return 0;
}

int consulta(char NomFitxer[], bool esborrats){
    Animal a1;
    FILE *f1;
    int n;
    system("cls || clear");
    //obrir per llegir amb binari. El fitxer ha d'existir.
    f1 = fopen(NomFitxer,"rb");
    if(f1 == NULL) return -1;
    system("cls || clear");
    while(!feof(f1)){
        n = fread(&a1,sizeof(Animal),1,f1);
        if(!feof(f1)){
            if(n == 0) return -3;
            if(!esborrats){
                if(a1.marcaEsborrat != '*')escriureAnimal(a1);
            }else{
                if(a1.marcaEsborrat == '*')escriureAnimal(a1);
            }
        }
    }
    fclose(f1);
    printf("\n"CONTINUAR"\n");
    getchar();
    return 0;
}
//esModifica = True fa la modificacio si es false fa la baixa
int baixaOModifica(char NomFitxer[], bool esModifica){
    system("cls || clear");
    Animal a1;
    FILE *f1;
    char nomTmp[MAX_NOM];
    int n = 0;
    f1 = fopen(NomFitxer,"rb+");
    if(f1 == NULL) return -1;
    if(esModifica == false){
        printf("\nIntrodueix el nom de l'animal per donar de Baixa: ");
    }else{printf("\nIntrodueix el nom de l'animal per Modificar: ");}
    scanf("%"xstr(MAX_NOM)"[^\n]",nomTmp);BB;
    while(!feof(f1)){
        n = fread(&a1,sizeof(Animal),1,f1);
        if(!feof(f1)){
            if(n == 0) return -3;
            if(a1.marcaEsborrat != '*' && strcmp(a1.nom,nomTmp) == 0){//Si no ¿Esta borrat? I trobat
                escriureAnimal(a1);
                if(!seguirBaixaOModifica(esModifica)) return -4;
                else{
                    if(fseek(f1, -(long) sizeof(Animal), SEEK_CUR)) return -2;//-1 posicion
                    if(esModifica == false){
                        a1.marcaEsborrat = '*';//Marco com borrat
                    }else{entrarAnimal(&a1, true);}
                    n = fwrite(&a1, sizeof(Animal), 1, f1);//Escriure en el disc
                    if(n == 0) return -3;//Control error
                    break;
                }
            }
        }
    }
    fclose(f1);
    printf("\n"CONTINUAR"\n");
    return 0;
}

bool seguirBaixaOModifica(bool esModifica){
    char opcio;
    bool segur;
    do{
        if(esModifica == false){
            printf("\nSegur que vols donar de Baixa?(s/n): ");
        }else{printf("\nSegur que vols Modificar?(s/n): ");}
        scanf("%c",&opcio);BB;
    }while(opcio != 's' && opcio != 'n');
    segur = (opcio == 's')?true:false;
    return segur;
}

int esborrarFitxer(char NomFitxer[]){
    return unlink(NomFitxer);
}

int compactarFitxer(char nomFitxer[]){
    system("cls || clear");
    Animal animal;
    int n;
    FILE *origen, *desti;
    origen = fopen(nomFitxer,"rb");
    if(origen == NULL) return -1;
    desti = fopen("tmp.dat","wb");
    if(desti == NULL) return -1;

    while(!feof(origen)){
        n = fread(&animal,sizeof(Animal),1,origen);
        if(!feof(origen)){
            if(n == 0) return -3;
            if(animal.marcaEsborrat != '*') n = fwrite(&animal, sizeof(Animal),1,desti);
        }
    }
    fclose(origen);
    fclose(desti);
    esborrarFitxer(nomFitxer);
    if(rename("tmp.dat",NOM_FITXER) == -1) return -5;
    return 0;
}

int esborrarFitxerGeneric(char nomFitxer[]){
    system("cls || clear");
    char opcio;
    int n = 0;
    do{
        printf("\nSegur que vols ELIMINAR PERMANENTMENT el fitxer?(s/n): ");
        scanf("%c",&opcio);BB;
    }while(opcio != 's' && opcio != 'n');
    if(opcio == 's'){
        n = esborrarFitxer(nomFitxer);
        if(n!=0) n = -6;
    }else n = -4;
    return n;
}

int numeroRegistres(char nomFitxer[], long *registre){
    system("cls || clear");
    Animal animal;
    long nReg = 0;
    int n;
    FILE *f1;

    f1 = fopen(nomFitxer,"rb");

    if(f1 == NULL) return -1;

    while(!feof(f1)){
        n = fread(&animal,sizeof(Animal),1,f1);
        if(!feof(f1)){
            if(n == 0){
                return -3;
            }
        }
    }
    fseek(f1, 0L, SEEK_END);
    nReg = ftell(f1);
    nReg = nReg/sizeof(Animal);
    fclose(f1);

    *registre = nReg;

    return 0;
}

int accesDirecte(char nomFitxer[]){
    system("cls || clear");
    Animal animal;
    int pos;
    int n,error;
    long total;
    error = numeroRegistres(NOM_FITXER, &total);
    if(error!=0) return -1;

    if(total == 0) return -7;

    do{
    printf("A quin registre vols accedir? MAX (%ld): ", total);
    scanf("%d", &pos);BB;
    }while(pos <= 0 || pos > total);

    FILE *f1;

    f1 = fopen(nomFitxer,"rb");

    if(f1 == NULL) return -1;

    n = pos-1;

    fseek(f1, (long)((n)*sizeof(Animal)), SEEK_SET);
    fread(&animal,sizeof(Animal),1,f1);
    if(animal.marcaEsborrat != '*'){
        escriureAnimal(animal);
    }else printf("\nRegistre no existeix o esta esborrat\n");


    fclose(f1);
    printf("\n"CONTINUAR"\n");
    getchar();
    return 0;
}

int crearInforme(char nomFitxer[]){
    FILE *fitxerHtml;
    FILE *fitxerAnimals;
    Animal a1;
    int n;
    char total[500];
    total[0] = '\0';
    char cadenaTmp[50];
    cadenaTmp[0] = '\0';

    char html[]= "<!DOCTYPE html> \n\
<html lang='es'> \n\
\t<head> \n\
\t\t<meta charset='UTF-8'> \n\
\t\t<meta name='viewport' content='width=device-width, initial-scale=1.0'> \n\
\t\t<title>INFORME ANIMALS</title> \n\
\t\t<link rel='stylesheet' href='index.css'> \n\
\t</head> \n\
\t<body> \n\
\t\t<h2>INFORMACI&Oacute; SOBRE ANIMALS</h2> \n\
\t\t<table> \n\
\t\t\t<th>Tipus</th> \n\
\t\t\t<th>Nom</th> \n\
\t\t\t<th>Sexe</th> \n\
\t\t\t<th>Perill Extincio</th> \n\
\t\t\t<th>Edat</th> \n\
\t\t\t<th>Pes</th>";

    char finalHTML[]="\n\t\t</table> \n\
\t</body> \n\
</html>\n \n";

    system("cls || clear");


    //obrir per llegir amb binari. El fitxer ha d'existir.
    fitxerHtml = fopen("index.html", "w");
    fitxerAnimals = fopen("animals.dat","r");
    if(fitxerAnimals == NULL){
        return -1;
    }
    if((fitxerHtml=fopen("index.html","w"))== NULL){
        return -1;
    }else{
        if(fputs(html,fitxerHtml) == EOF){
            return -2;
        }
    }
    while(!feof(fitxerAnimals)){
        n = fread(&a1,sizeof(Animal),1,fitxerAnimals);
        if(!feof(fitxerAnimals)){
            if(n == 0){
                return -3;
            }
            if(a1.marcaEsborrat != '*'){
                total[0] = '\0';
                strcat(total, "\n\t\t\t<tr>"); //transformar i esciure
                strcat(total, "\n\t\t\t\t<td>");
                strcat(total, a1.tipus);
                strcat(total, "</td>");
                strcat(total, "\n\t\t\t\t<td>");
                strcat(total, a1.nom);
                strcat(total, "</td>");
                strcat(total, "\n\t\t\t\t<td>");
                if(a1.sexe == 'm') strcpy(cadenaTmp,"Mascle");
                else strcpy(cadenaTmp,"Femella");
                strcat(total, cadenaTmp);
                strcat(total, "</td>");
                strcat(total, "\n\t\t\t\t<td>");
                if(a1.enPerillExtincio) strcpy(cadenaTmp,"Si");
                else strcpy(cadenaTmp,"No");
                strcat(total, cadenaTmp);
                strcat(total, "</td>");
                strcat(total, "\n\t\t\t\t<td>");
                sprintf(cadenaTmp,"%d",a1.edat);
                strcat(total, cadenaTmp);
                strcat(total, "</td>");
                strcat(total, "\n\t\t\t\t<td>");
                sprintf(cadenaTmp,"%.2lf",a1.pes);
                strcat(total, cadenaTmp);
                strcat(total, "</td>");
                strcat(total, "\n\t\t\t</tr>");
                fputs(total,fitxerHtml);
            }
        }
    }
    fputs(finalHTML,fitxerHtml);//Escriure final html
    fclose(fitxerAnimals);
    if(fclose(fitxerHtml)){
        return -4;
    }else printf(INFO_NO_ERROR);
    fflush(stdout);
    system("firefox index.html");

    return 0;
}
