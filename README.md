Arnau Inglada Francés

# Gestió de fitxers en C #

En aquest exercici analitzarem la gestió de fitxers binaris amb accés seqüencial.
Entenem per gestió: l’alta, la baixa, la consulta i la modificació de registres, l’esborrat del fitxer i la compactació del fitxer. Aquesta darrera opció elimina, de forma definitiva, els registres marcats per esborrar.
També  inclourem les opcións de comptar el numero total de registres (borrats i no borrats), un accés directe als registres visibles i una opció per generar un informe amb una pagina web HTML.

## Definicions ##

>Dada Animal (struct)
>>El struct animal conte la informació de l'animal, el camp char nom[] es la clau primaria del struct. Aixó vol dir que nomes pot haver un nom únic. El camp char marcaEsborrat indica amb un * quins animals estan donats de baixa.

>Pintar Menu (void)
>>Fa printf de les accions que l'usuari pot escollir per utilitzar. fflush(stdout) sincronitza la terminal i system("cls || clear") deixa la terminal neta.

>Entrar Opció (void)
>>L'usuari defineix quina utilitat del programa vol utilitzar. 

>Entrar Animal (void)
>>Defineix els valors dels camps d'Animal mitjançant scanf().

>Escriure Animal (void)
>>Mostra els valors d'Animal.

>Alta, Consulta, BaixaOModifica, Compactar Fitxer, Esborrar Fitxer Generic, Numero Registres, Accés Directe i Crear Informe (int)
>>Retorna un valor int si aquest valor es igual a 0 es que tot a funcionat correctament. Hi ha codi de errors.

>Seguir BaixaOModifica (bool)
>>Retorna un valor boolea depenent de si l'usuari vol tirar endavant amb l'acció o no.

>Esborrar Fitxer (int)
>>Borra completament el fitxer on es guarda la informació d'Animals.

#### Codi d'Errors ####
- 0: No hi ha hagut cap error
- -1: Error en obrir el fitxer
- -2: Error en escriure al fitxer
- -3: Error de lectura de fitxer
- -4: Operació Cancelada
- -5: Error al renombrar el fitxer
- -6: Error al borrar el fitxer
- -7: Operació no disponible

## MAIN ##

- opcio (int): Emmagatzema el valor escollit per l'usuari per definir quina variant del programa s'utilitzarà.

- error (int): Emmagatzema el valor que retornen les accions per generar el codi d'errors.

- switch(opcio): Determina que s'ha de realitzar en cas de que l'usuari escrigui a opcio qualsevol de les opcions preestablertes, mentre opcio tingui un valor de 0 a 10. En cas de que el valor sigui 0 el programa acaba.

## Alta ##

- Defineix f1 al fitxer, a1 com a registre de Animal i n(int) per fer control de errors.

- Obra el fitxer en format ab (així si el fitxer ja esta existeix continua a continuació i sinó el crea)

`f1 = fopen(nomFitxer,"ab");`

- Fa comprovacions de error si el fitxer no existeix es que no s'ha obert bé.

`if(f1 == NULL) return -1;`

- Entra la infromació d'Animal i escriu al fitxer.

`n = fwrite(&a1,sizeof(Animal),1,f1);`

- En cas de que n valgui 0 (no ha escrit res) retorna error.

`if(n == 0) return -2;`

- Tanca el fitxer.

`fclose(f1);`

## Consulta ##

- Inicialitza com a Alta.

- Obra el fitxer en mode lectura ja que nomes volem veure el contingut.

`f1 = fopen(NomFitxer,"rb");`

- Mentre no sigui final de fitxer llegeix cada regisre d'Animal i guarda el resultat a n. Depenent de si volem consultar els registres actius o els registres donats de baixa (previa definició al MAIN), escriu els registres que no tenen un * al camp a1.marcaEsborrat (actius) o els que si tenen un *(baixes).

![Imatge de la definició anterior](assets/1.PNG)

- Si no es final de fitxer comprova errors de lectura (si n val 0 no ha llegit res).

`if(n == 0) return -3;`

## BaixaOModifica ##

- Inicialitza com Alta afegint un char nomTMP[MAX_NOM]

- Obrim el fitxer en mode rb+ perque volem llegir pero tambe es possible que escriguem. Controla errors.

`f1 = fopen(NomFitxer,"rb+");`

- Si esModifica es false (previa definició al MAIN), pregunta el nom de l'Animal per marcar * al camp marcaEsborrat de tal animal. Si es esModifica es true pregunta el nom de l'Animal per mostrar la info del Animal i sobreescriure-la.

- Per saber el nom del Animal utilitzem la comanda strcmp aixi podem parar al registre que volem (tindrem que reposciconar el apuntador perque al acabar de llegir un registre es queda al final). Es necessaria la llibreia de string.h

`if(a1.marcaEsborrat != '*' && strcmp(a1.nom,nomTmp) == 0){`Si no ¿Esta borrat? I trobat

- En abans de fer cap modificació hi ha un control de verificació, per saber si l'usuari esta d'acord en fer el canvi corresponent.

`if(!seguirBaixaOModifica(esModifica)) return -4;`

## Esborrar Fitxer ##

- Retorna el valor de borrar el fixer.

`return unlink(NomFitxer);`

## Compactar Fitxer ##

- Crea un fitxer nou temporal, llegeix el fitxer on es guarden els animals i despres escriu els registres NO BORRATS al fitxer temporal. Finalment esborra el fitxer original i renombra el fitxer temporal posant-li el nom del fitxer original. Amb comprovació de error per el renom.

`if(rename("tmp.dat",NOM_FITXER) == -1) return -5;`

## Esborrar Fitxer Generic ##

- Crida la funcio Esborrar Fitxer i fa una una verificació per a que l'usuari confirmi l'ordre.

- Control de error

`n = esborrarFitxer(nomFitxer);`

`if(n!=0) n = -6;`

## Numero Registres ## 

- Utilitzem una variable long (ja que utilitzarem una comanda que retorna aquest valor) que emmagatzemara finalment el total de registres.

- Ens posicionem al final del fitxer amb tota la infromació dels Animals per fer un ftell()(que compta la cuantitat de Bytes des de l'inici del fitxer fins a la posició actual)

`nReg = ftell(f1);`

- Amb el resultat de l'anterior comanda fem una operació per saber quants registres hi ha (quant ocupa un registre en bytes/total de bytes).

`nReg = nReg/sizeof(Animal);`

## Accés Directe ##

- Inicialitzem una variable pos per guardar la posició. Posteriorment li tindrem que restar 1 per poder colocar-ho bé als registres. També una variable total que es el resultat de la funció Numero Registres.

- El usuari indica al registre NUMERIC que es vol desplaçar. Previament hem bloquejat les opcions de l'usuari per tal de que no sobrepasi la quantitat maxima de registres.

`do{}while(pos <= 0 || pos > total);`

- En el cas de que el registre seleccionat estigui marcat com a esborrat mostra un missatje de error.

`if(animal.marcaEsborrat != '*'){escriureAnimal(animal);}else printf (n"Registre no existeix o esta esborrat"n);`

## Crear Informe ##

- Creem un fitxer de text amb format w i obrim el fitxer amb la informació dels animals amb mode text/lectura r.

`fitxerHtml = fopen("index.html", "w");`

`fitxerAnimals = fopen("animals.dat","r");`

- Al fitxer html posem una cadena char html[] amb la capçalera de html i fins a la creació de la taula.

- Mentre no sigui final de fitxer si el registre no esta marcat com esborrat converteix la informació binaria a text i copia al html. (necessitem la llibreia unistd.h).

`strcat(total, a1.tipus);`.

`if(a1.enPerillExtincio) strcpy(cadenaTmp,"Si");else strcpy(cadenaTmp,"No");`

`sprintf(cadenaTmp,"%d",a1.edat);strcat(total, cadenaTmp);`

`fputs(total,fitxerHtml);`

- Posem el peu de HTML.




